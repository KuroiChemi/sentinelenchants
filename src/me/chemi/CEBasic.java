package me.chemi;

import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.HashSet;

public abstract class CEBasic {
	protected final HashSet<Trigger> triggers = new HashSet<>();
	protected final HashMap<PotionEffectType, Integer> potionsOnWear = new HashMap<>();
	protected String displayName;
	
	public String getDisplayName()
	{
		return this.displayName;
	}
	
	public HashSet<Trigger> getTriggers()
	{
		return this.triggers;
	}
	
	public HashMap<PotionEffectType, Integer> getPotionEffectsOnWear()
	{
		return this.potionsOnWear;
	}

public enum Trigger {
	DAMAGE_GIVEN, DAMAGE_TAKEN, DEATH, WEAR_ITEM, BLOCK_BROKEN, MOVE, BOW_SHOOT, ARMOR_BREAK, FOOD_CHANGE
}
}