package me.chemi.Enchantments.Helmet;

import me.chemi.CEMain;
import me.chemi.Enchantments.CEnchantment;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class Hellfire
		extends CEnchantment {
	
	public Hellfire()
	{
		super(Application.HELMET, Tier.III, Tier.IIII);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		Player p = (Player) event.getEntity();
		
		if (generateChance(5))
		{
			new BukkitRunnable() {
				int count = (level == 3) ? 3 : 5;
				
				@Override
				public void run()
				{
					if (count <= 0)
						cancel();
					count--;
					Fireball f = p.launchProjectile(Fireball.class);
					f.setIsIncendiary(false);
				}
			}.runTaskTimer(CEMain.plugin, 5L, 20L);
		}
	}
}
