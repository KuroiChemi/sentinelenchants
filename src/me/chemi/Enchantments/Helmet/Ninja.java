package me.chemi.Enchantments.Helmet;

import me.chemi.Enchantments.CEnchantment;
import me.chemi.Utils.Utils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Ninja
		extends CEnchantment {
	
	public Ninja()
	{
		super(Application.BOOTS, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		LivingEntity damager = Utils.getDamager(event);
		
		Player p = (Player) event.getEntity();
		if (p.getHealth() < (p.getMaxHealth() / 5))
		{
			if (generateChance(10 * level))
			{
				damager.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 1));
				damager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100, 2));
			}
		}
		
	}
}