package me.chemi.Enchantments.Helmet;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Nightvision
		extends CEnchantment {
	
	public Nightvision()
	{
		super(Application.HELMET, Tier.III);
		this.triggers.add(Trigger.WEAR_ITEM);
		this.potionsOnWear.put(PotionEffectType.NIGHT_VISION, 1);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
	}
	
}