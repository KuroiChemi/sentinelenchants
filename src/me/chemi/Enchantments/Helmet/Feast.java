package me.chemi.Enchantments.Helmet;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.inventory.ItemStack;

public class Feast
		extends CEnchantment {
	
	public Feast()
	{
		super(Application.HELMET, Tier.I);
		this.triggers.add(Trigger.FOOD_CHANGE);
		this.triggers.add(Trigger.WEAR_ITEM);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		FoodLevelChangeEvent event = (FoodLevelChangeEvent) e;
		event.setCancelled(true);
	}
	
}