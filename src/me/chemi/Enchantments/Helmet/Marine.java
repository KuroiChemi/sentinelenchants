package me.chemi.Enchantments.Helmet;

import me.chemi.CEBasic;
import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Marine
		extends CEnchantment {
	
	public Marine()
	{
		super(Application.HELMET, Tier.I);
		this.triggers.add(CEBasic.Trigger.WEAR_ITEM);
		this.potionsOnWear.put(PotionEffectType.WATER_BREATHING, 1);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
	}
}
