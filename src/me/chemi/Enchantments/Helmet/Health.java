package me.chemi.Enchantments.Helmet;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Health
		extends CEnchantment {
	
	public Health()
	{
		super(Application.HELMET, Tier.I, Tier.II, Tier.III);
		triggers.add(Trigger.WEAR_ITEM);
		potionsOnWear.put(PotionEffectType.HEALTH_BOOST, -1);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
	}
}
