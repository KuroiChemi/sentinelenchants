package me.chemi.Enchantments.Armor;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Poisoned
		extends CEnchantment {
	
	public Poisoned()
	{
		super(Application.ARMOR, Tier.I, Tier.II, Tier.III, Tier.IIII);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		Player p = (Player) event.getEntity();
		
		int duration = 100;
		
		switch (level)
		{
			case 1:
				break;
			case 2:
				duration += 20;
				break;
			default:
				duration += 60;
		}
		
		if (generateChance((level % 2 == 0) ? 35 : 30))
		{
			p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, duration, (level > 2) ? 2 : 1), true);
		}
	}
}
