package me.chemi.Enchantments.Armor;

import me.chemi.CEMain;
import me.chemi.Enchantments.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class Hardened
		extends CEnchantment {
	
	public Hardened()
	{
		super(Application.ARMOR, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.ARMOR_BREAK);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		PlayerItemBreakEvent event = (PlayerItemBreakEvent) e;
		
		int save = item.getDurability() - 30;
		
		if (level == 2)
			save -= 30;
		else if (level == 3)
			save -= 50;
		
		Player p = event.getPlayer();
		item.setAmount(1);
		
		int finalSave = save;
		new BukkitRunnable() {
			@Override
			public void run()
			{
				if (item.getType().toString().endsWith("HELMET"))
				{
					p.getInventory().setHelmet(item);
					p.getInventory().getHelmet().setDurability((short) finalSave);
				}
				else if (item.getType().toString().endsWith("CHESTPLATE"))
				{
					p.getInventory().setChestplate(item);
					p.getInventory().getChestplate().setDurability((short) finalSave);
				}
				else if (item.getType().toString().endsWith("LEGGINGS"))
				{
					p.getInventory().setLeggings(item);
					p.getInventory().getLeggings().setDurability((short) finalSave);
				}
				else
				{
					p.getInventory().setBoots(item);
					p.getInventory().getBoots().setDurability((short) finalSave);
				}
			}
		}.runTaskLater(CEMain.plugin, 2L);
	}
}
