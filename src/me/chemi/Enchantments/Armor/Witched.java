package me.chemi.Enchantments.Armor;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Witched
		extends CEnchantment {
	
	public Witched()
	{
		super(Application.ARMOR, Tier.I, Tier.II, Tier.III, Tier.IIII);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		
		int duration = (level % 2 == 0) ? 160 : (level == 3) ? 120 : 100;
		
		if (generateChance(5))
		{
			((LivingEntity) event.getDamager()).addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, duration, (level > 2) ? 2 : 1), true);
		}
	}
}
