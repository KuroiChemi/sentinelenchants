package me.chemi.Enchantments.Armor;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Regen
		extends CEnchantment {
	
	public Regen()
	{
		super(Application.ARMOR, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.WEAR_ITEM);
		this.potionsOnWear.put(PotionEffectType.REGENERATION, -1);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
	}
}
