package me.chemi.Enchantments.Pickaxe;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

import static me.chemi.Utils.Utils.isUsable;

public class Blast
		extends CEnchantment {
	
	public Blast()
	{
		super(Application.PICKAXE, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.BLOCK_BROKEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		BlockBreakEvent event = (BlockBreakEvent) e;
		if (!isUsable(event.getBlock().getType().toString()))
		{
			return;
		}
		
		Location sL = event.getBlock().getLocation();
		List<Block> blockList = getNearbyBlocks(sL, level);
		short durability = (short) (item.getDurability() + blockList.size());
		
		blockList.stream().filter(b -> !b.getDrops(item).isEmpty()).forEach(b ->
				                                                                    b.breakNaturally(item));
		
		Player p = event.getPlayer();
		if (aboutToBreak(item.getType(), durability))
			p.getInventory().setItem(p.getInventory().getHeldItemSlot(), null);
		else
			item.setDurability(durability);
	}
	
	private List<Block> getNearbyBlocks(Location location, int Radius)
	{
		List<Block> Blocks = new ArrayList<>();
		for (int X = location.getBlockX() - Radius; X <= location.getBlockX() + Radius; X++)
		{
			for (int Y = location.getBlockY() - Radius; Y <= location.getBlockY() + Radius; Y++)
			{
				for (int Z = location.getBlockZ() - Radius; Z <= location.getBlockZ() + Radius; Z++)
				{
					Block block = location.getWorld().getBlockAt(X, Y, Z);
					if (isUsable(block.getType().toString()))
						Blocks.add(block);
				}
			}
		}
		return Blocks;
	}
	
	private boolean aboutToBreak(Material m, int durability)
	{
		return (m == Material.DIAMOND_PICKAXE && durability > 1562) || (m == Material.IRON_PICKAXE && durability > 251 ||
				                                                                (m == Material.STONE_PICKAXE && durability > 132) || (m == Material.WOOD_PICKAXE && durability > 60) ||
				                                                                (m == Material.GOLD_PICKAXE && durability > 33));
	}
}
