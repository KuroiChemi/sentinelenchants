package me.chemi.Enchantments.Pickaxe;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class Smelt
		extends CEnchantment {
	
	public Smelt()
	{
		super(Application.PICKAXE, Tier.II);
		this.triggers.add(Trigger.BLOCK_BROKEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		BlockBreakEvent event = (BlockBreakEvent) e;
		Player player = event.getPlayer();
		if (!event.getBlock().getDrops(item).isEmpty())
		{
			Material drop = null;
			Block b = event.getBlock();
			Material m = b.getType();
			
			if (m == Material.STONE)
			{
				drop = m;
			}
			else if (m == Material.COBBLESTONE)
			{
				drop = Material.STONE;
			}
			else if (m == Material.IRON_ORE)
			{
				drop = Material.IRON_INGOT;
			}
			else if (m == Material.GOLD_ORE)
			{
				drop = Material.GOLD_INGOT;
			}
			else if (m == Material.SAND)
			{
				drop = Material.GLASS;
			}
			else if (m == Material.CLAY)
			{
				drop = Material.BRICK;
			}
			if (drop != null)
			{
				event.setCancelled(true);
				player.getWorld().dropItemNaturally(b.getLocation(), new ItemStack(drop, event.getBlock().getDrops(player.getItemInHand()).size()));
				b.setType(Material.AIR);
			}
		}
	}
}
