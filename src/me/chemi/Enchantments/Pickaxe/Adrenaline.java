package me.chemi.Enchantments.Pickaxe;

import me.chemi.Enchantments.CEnchantment;
import me.chemi.Utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Adrenaline
		extends CEnchantment {
	
	public Adrenaline()
	{
		super(Application.PICKAXE, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.BLOCK_BROKEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		BlockBreakEvent event = (BlockBreakEvent) e;
		Player player = event.getPlayer();
		if (Utils.isUsable(event.getBlock().getType().toString()))
			player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 60, level - 1), false);
	}
}