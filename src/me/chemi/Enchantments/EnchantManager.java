package me.chemi.Enchantments;

import me.chemi.Enchantments.CEnchantment.Tier;
import me.chemi.Utils.Utils;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.stream.Collectors;

public class EnchantManager {
	
	private static final Enchantment glowEnchantment = GlowEnchantment.getGlow();
	private static final Set<CEnchantment> enchantments = new LinkedHashSet<>();
	private static final String bookName = Utils.color("&b&lMagic &lBook");
	
	public static ItemStack addEnchant(ItemStack item, CEnchantment ce, int level, Player p)
	{
		ItemMeta im = item.getItemMeta();
		List<String> lore = new ArrayList<>();
		
		if (im.hasLore())
			lore = im.getLore();
		
		if (getEnchantments(lore).size() + 1 > getEnchantmentsAllowed(p))
		{
			item.setType(Material.BARRIER);
			return item;
		}
		
		if (level > ce.getEnchantmentMaxLevel())
		{
			level = ce.getEnchantmentMaxLevel();
		}
		
		removeEnchant(lore, ce);
		lore.add(getLevelColor(ce.getTiers(), level) + ce.getDisplayName() + " " + intToLevel(level));
		im.setLore(lore);
		item.setItemMeta(im);
		item.addUnsafeEnchantment(glowEnchantment, 1);
		return item;
	}
	
	private static int getEnchantmentsAllowed(Player p)
	{
		int count = 10;
		for (; count > 0; count--)
		{
			if (p.hasPermission("SentinelEnchants." + count))
				return count;
		}
		return 0;
	}
	
	public static Set<CEnchantment> getEnchantments(List<String> lore)
	{
		Set<CEnchantment> list = new LinkedHashSet<>();
		if (lore != null)
		{
			for (String name : lore)
			{
				CEnchantment ce = getEnchantment(name);
				if (ce != null)
					list.add(ce);
			}
		}
		return list;
	}
	
	private static void removeEnchant(List<String> lore, CEnchantment ce)
	{
		lore.removeIf(x -> ChatColor.stripColor(x).startsWith(ce.getDisplayName()));
	}
	
	private static ChatColor getLevelColor(List<Tier> tierList, int level)
	{
		Tier tier;
		
		if (tierList.size() == 1)
			tier = tierList.get(0);
		else if (tierList.size() == 2)
			tier = tierList.get(tierList.contains(Tier.I) ? level - 1 : (level == 1) ? 0 : (level / 2) - 1);
		else if (tierList.size() == 3)
			tier = tierList.get(tierList.contains(Tier.I) ? level - 1 : level - 2);
		else
			tier = tierList.get(level - 1);
		
		switch (tier)
		{
			case I:
				return ChatColor.GREEN;
			case II:
				return ChatColor.AQUA;
			case III:
				return ChatColor.RED;
			default:
				return ChatColor.GOLD;
		}
	}
	
	private static String intToLevel(int i)
	{
		switch (i)
		{
			case 1:
				return "I";
			case 2:
				return "II";
			case 3:
				return "III";
			default:
				return "IV";
		}
	}
	
	public static CEnchantment getEnchantment(String name)
	{
		if (name.length() > 3)
		{
			for (CEnchantment ce : enchantments)
			{
				String enchantment = ChatColor.stripColor(ce.getDisplayName()).toLowerCase();
				name = ChatColor.stripColor(name).toLowerCase();
				if ((name.startsWith(enchantment)) || (name.startsWith(ce.getDisplayName().toLowerCase())))
				{
					String[] split = name.split(" ");
					if (split.length == enchantment.split(" ").length + 1)
					{
						name = name.substring(0, name.length() - 1 - split[(split.length - 1)].length());
						if ((name.equals(enchantment)) || (name.equals(ce.getDisplayName())))
						{
							return ce;
						}
					}
					else if ((name.equals(enchantment)) || (name.equals(ce.getDisplayName())))
					{
						return ce;
					}
				}
			}
		}
		return null;
	}
	
	public static boolean isEnchantmentBook(ItemStack i)
	{
		if (i != null && i.getType().equals(Material.ENCHANTED_BOOK))
			if (i.hasItemMeta() && i.getItemMeta().hasDisplayName() && i.getItemMeta().getDisplayName().equals(bookName))
				return true;
		return false;
	}
	
	public static List<CEnchantment> getTierEnchantments(CEnchantment.Tier tier)
	{
		return enchantments.stream().filter(ce -> ce.getTiers().contains(tier)).collect(Collectors.toList());
	}
	
	public static Set<CEnchantment> getEnchantments()
	{
		return enchantments;
	}
	
	public static HashMap<CEnchantment, Integer> getEnchantmentLevels(List<CEnchantment> enchants, List<String> lore)
	{
		HashMap<CEnchantment, Integer> list = new HashMap<>();
		if (lore != null)
		{
			for (String name : lore)
			{
				getLevel(name);
				if (name.length() > 3)
				{
					for (CEnchantment ce : enchants)
					{
						name = ChatColor.stripColor(name).toLowerCase();
						String enchantment = ChatColor.stripColor(ce.getDisplayName()).toLowerCase();
						
						if ((name.startsWith(enchantment)))
						{
							String[] split = name.split(" ");
							if (split[0].equals(enchantment) || split[0].equals(ce.getDisplayName()))
							{
								list.put(ce, levelToInt(split[split.length - 1]));
							}
						}
					}
				}
			}
		}
		return list;
	}
	
	public static int getLevel(String checkEnchant)
	{
		int level = 1;
		if (checkEnchant.contains(" "))
		{
			String[] splitName = checkEnchant.split(" ");
			String possibleLevel = splitName[(splitName.length - 1)];
			level = levelToInt(possibleLevel);
		}
		return level;
	}
	
	static int levelToInt(String level)
	{
		switch (level.toUpperCase())
		{
			case "I":
				return 1;
			case "II":
				return 2;
			case "III":
				return 3;
			default:
				return 4;
		}
	}
	
	public static boolean containsEnchantment(String toTest, CEnchantment ce)
	{
		String next = ce.getDisplayName();
		
		return !next.isEmpty() && ChatColor.stripColor(toTest).startsWith(next);
		
	}
	
	public static ItemStack getEnchantBook(CEnchantment ce, int level)
	{
		String app = WordUtils.capitalize(ce.getApplication().name().toLowerCase());
		
		if (app.equalsIgnoreCase("Insanity"))
			app = "Chestplate and Leggings";
		
		ItemStack item = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta im = item.getItemMeta();
		im.setLore(Arrays.asList(getLevelColor(ce.getTiers(), level) + ce.getDisplayName() + " " + intToLevel(level), ChatColor.GRAY + app));
		im.setDisplayName(bookName);
		item.setItemMeta(im);
		item.addUnsafeEnchantment(glowEnchantment, 0);
		return item;
	}
	
	public static boolean isEnchantable(String mat)
	{
		return mat.contains("HELMET") || mat.contains("CHESTPLATE") || mat.contains("LEGGINGS") || mat.contains("BOOTS") || mat.contains("SWORD") || mat.contains("AXE") || mat.equals("BOW") || mat.contains("PICKAXE");
	}
	
	public static boolean isViable(CEnchantment ce, int level)
	{
		return level <= ce.getEnchantmentMaxLevel() && level >= levelToInt(ce.getTiers().get(0).toString());
	}
}
