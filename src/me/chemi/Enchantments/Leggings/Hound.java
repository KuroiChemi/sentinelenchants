package me.chemi.Enchantments.Leggings;

import me.chemi.CEMain;
import me.chemi.Enchantments.CEnchantment;
import me.chemi.Utils.Utils;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Wolf;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

public class Hound
		extends CEnchantment {
	
	public Hound()
	{
		super(Application.LEGGINGS, Tier.I, Tier.II, Tier.III, Tier.IIII);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		LivingEntity damager = Utils.getDamager(event);
		
		if (generateChance(5))
		{
			Wolf wolf = (Wolf) damager.getWorld().spawnEntity(event.getEntity().getLocation(), EntityType.WOLF);
			wolf.setMaxHealth((level < 3) ? level * 5 + 5 : 40 + level * 10);
			wolf.setHealth(wolf.getMaxHealth());
			wolf.setAngry(true);
			wolf.setTarget(damager);
			
			wolf.setMetadata("owner", new FixedMetadataValue(CEMain.plugin, event.getEntity().getUniqueId().toString()));
			
			if (level > 1)
				wolf.setMetadata("extraDamage", new FixedMetadataValue(CEMain.plugin, 2 + level));
			
			new BukkitRunnable() {
				@Override
				public void run()
				{
					wolf.remove();
				}
			}.runTaskLater(CEMain.plugin, 500L);
		}
	}
}
