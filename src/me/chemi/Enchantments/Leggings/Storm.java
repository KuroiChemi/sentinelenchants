package me.chemi.Enchantments.Leggings;

import me.chemi.CEMain;
import me.chemi.Enchantments.CEnchantment;
import me.chemi.Utils.Utils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class Storm
		extends CEnchantment

{
	
	public Storm()
	{
		super(Application.LEGGINGS, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.DAMAGE_GIVEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		LivingEntity damager = Utils.getDamager(event);
		
		new BukkitRunnable() {
			int count = level;
			
			@Override
			public void run()
			{
				if (generateChance(20))
				{
					damager.getWorld().strikeLightning(
							event.getEntity().getLocation());
				}
				else
					cancel();
				
				if (count-- <= 0)
					cancel();
			}
		}.runTaskTimer(CEMain.plugin, 3L, 5L);
	}
}
