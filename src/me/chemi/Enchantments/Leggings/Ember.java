package me.chemi.Enchantments.Leggings;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Ember
		extends CEnchantment {
	
	public Ember()
	{
		super(Application.LEGGINGS, Tier.I);
		this.triggers.add(Trigger.WEAR_ITEM);
		this.potionsOnWear.put(PotionEffectType.FIRE_RESISTANCE, 1);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
	}
}
