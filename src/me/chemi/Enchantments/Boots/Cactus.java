package me.chemi.Enchantments.Boots;

import me.chemi.CEMain;
import me.chemi.Enchantments.CEnchantment;
import me.chemi.Enchantments.Weapons.Combo;
import me.chemi.Enchantments.Weapons.Combo.Cooldown;
import me.chemi.Utils.Utils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Cactus
		extends CEnchantment {
	
	private final List<UUID> bleedList = new ArrayList<>();
	
	public Cactus()
	{
		super(Application.BOOTS, Tier.I, Tier.II, Tier.III, Tier.IIII);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		LivingEntity damager = Utils.getDamager((EntityDamageByEntityEvent) e);
		
		if (bleedList.contains(damager.getUniqueId()))
			return;
		
		if (generateChance(15 + (5 * level)))
		{
			bleedList.add(damager.getUniqueId());
			new BukkitRunnable() {
				int count = 5;
				
				@Override
				public void run()
				{
					if (damager.isDead() || count <= 0)
					{
						bleedList.remove(damager.getUniqueId());
						cancel();
					}
					
					if (Combo.cooldown.containsKey(damager.getUniqueId()))
						Combo.cooldown.replace(damager.getUniqueId(), new Cooldown());
					damager.damage(1D);
					count--;
				}
			}.runTaskTimer(CEMain.plugin, 10L, 20L);
		}
		
	}
}
