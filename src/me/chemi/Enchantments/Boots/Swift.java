package me.chemi.Enchantments.Boots;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Swift
		extends CEnchantment {
	
	public Swift()
	{
		super(Application.BOOTS, Tier.II, Tier.III);
		this.triggers.add(Trigger.WEAR_ITEM);
		this.potionsOnWear.put(PotionEffectType.SPEED, -1);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
	}
	
}
