package me.chemi.Enchantments.Boots;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Escape
		extends CEnchantment {
	
	public Escape()
	{
		super(Application.BOOTS, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		
		int chance = 40;
		
		if (level > 1)
			chance = 50 + ((level - 2) * 20);
		
		Player p = (Player) event.getEntity();
		if (p.getHealth() < (p.getMaxHealth() / 5))
		{
			if (generateChance(chance))
			{
				p.setVelocity(p.getEyeLocation().getDirection().normalize().multiply(level * 5).setY(1));
				p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 100, level + 3), true);
			}
		}
		
	}
}
