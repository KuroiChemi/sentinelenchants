package me.chemi.Enchantments.Boots;

import me.chemi.Enchantments.CEnchantment;
import me.chemi.Utils.Utils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

public class Flee
		extends CEnchantment {
	
	public Flee()
	{
		super(Application.BOOTS, Tier.I, Tier.II, Tier.III, Tier.IIII);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		LivingEntity damager = Utils.getDamager((EntityDamageByEntityEvent) e);
		
		if (generateChance(10))
		{
			damager.setVelocity(damager.getEyeLocation().getDirection().multiply(-1).setY(1));
		}
	}
}