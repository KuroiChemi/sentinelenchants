package me.chemi.Enchantments.Boots;

import me.chemi.CEMain;
import me.chemi.Enchantments.CEnchantment;
import me.chemi.Utils.Utils;
import me.dablakbandit.customentitiesapi.entities.CustomEntities;
import me.dablakbandit.customentitiesapi.entities.CustomEntityZombie;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Zombie;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class Swarm
		extends CEnchantment {
	
	public static final Map<UUID, Attacked> spawned = new HashMap<>();
	
	public Swarm()
	{
		super(Application.BOOTS, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		LivingEntity damager = Utils.getDamager(event);
		
		if (spawned.containsKey(event.getEntity().getUniqueId()))
			return;
		
		if (generateChance(10))
		{
			List<UUID> minions = new ArrayList<>();
			
			int count = level + 2;
			
			while (count > 0)
			{
				CustomEntityZombie cez = CustomEntities.getNewCustomEntityZombie(event.getEntity().getLocation());
				cez.removeGoalSelectorPathfinderGoalAll();
				cez.newGoalSelectorPathfinderGoalMeleeAttack(1, true);
				cez.setBaby(true);
				Zombie z = (Zombie) cez.getBukkitEntity();
				z.setTarget(damager);
				minions.add(z.getUniqueId());
				count--;
			}
			
			spawned.put(event.getEntity().getUniqueId(), new Attacked(minions, damager));
			
			new BukkitRunnable() {
				@Override
				public void run()
				{
					event.getEntity().getNearbyEntities(30, 30, 30).stream().filter(e -> e instanceof Zombie && minions.contains(e.getUniqueId())).forEach(Entity::remove);
					spawned.remove(event.getEntity().getUniqueId());
				}
			}.runTaskLater(CEMain.plugin, 120L);
		}
	}
	
	public class Attacked {
		final Entity attacked;
		final List<UUID> minions;
		
		Attacked(List<UUID> minions, Entity attacked)
		{
			this.minions = minions;
			this.attacked = attacked;
		}
		
		public Entity getAttacked()
		{
			return attacked;
		}
		
		public List<UUID> getMinions()
		{
			return minions;
		}
	}
}
