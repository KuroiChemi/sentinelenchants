package me.chemi.Enchantments.Boots;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import me.chemi.Enchantments.CEnchantment;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

public class Wings
		extends CEnchantment {
	
	public Wings()
	{
		super(Application.BOOTS, Tier.III);
		this.triggers.add(Trigger.MOVE);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		PlayerMoveEvent event = (PlayerMoveEvent) e;
		
		Player p = event.getPlayer();
		
		if (p.getGameMode() == GameMode.CREATIVE)
			return;
		
		FPlayer fp = FPlayers.getInstance().getByPlayer(p);
		
		if (p.getAllowFlight() && !fp.isInOwnTerritory())
		{
			p.setAllowFlight(false);
			p.setFlying(false);
		}
		else
			p.setAllowFlight(true);
	}
	
}
