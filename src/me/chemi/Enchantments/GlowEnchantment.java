package me.chemi.Enchantments;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;

class GlowEnchantment extends EnchantmentWrapper {
	
	private static Enchantment glow;
	
	private GlowEnchantment()
	{
		super(255);
	}
	
	public static Enchantment getGlow()
	{
		if (glow != null)
			return glow;
		
		try
		{
			Field f = Enchantment.class.getDeclaredField("acceptingNew");
			f.setAccessible(true);
			f.set(null, true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		glow = new GlowEnchantment();
		
		if (Enchantment.getById(255) == null)
			Enchantment.registerEnchantment(glow);
		
		return glow;
	}
	
	@Override
	public int getMaxLevel()
	{
		return 10;
	}
	
	@Override
	public int getStartLevel()
	{
		return 1;
	}
	
	@Override
	public EnchantmentTarget getItemTarget()
	{
		return null;
	}
	
	@Override
	public boolean canEnchantItem(ItemStack item)
	{
		return true;
	}
	
	@Override
	public String getName()
	{
		return "Glow";
	}
	
	@Override
	public boolean conflictsWith(Enchantment other)
	{
		return false;
	}
	
}