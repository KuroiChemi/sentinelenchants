package me.chemi.Enchantments.Weapons;

import me.chemi.Enchantments.CEnchantment;
import me.chemi.Utils.Utils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Combo
		extends CEnchantment {
	
	public static final Map<UUID, Cooldown> cooldown = new HashMap<>();
	
	public Combo()
	{
		super(Application.WEAPON, Tier.III);
		this.triggers.add(Trigger.DAMAGE_GIVEN);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		LivingEntity damager = Utils.getDamager((EntityDamageByEntityEvent) e);
		
		UUID uuid = damager.getUniqueId();
		
		if (!cooldown.containsKey(uuid))
			cooldown.put(uuid, new Cooldown());
		
		Cooldown values = cooldown.get(uuid);
		
		if (values.getCooldown() + 4000L < System.currentTimeMillis())
		{
			cooldown.replace(uuid, new Cooldown(1));
			return;
		}
		
		if (values.getCombo() < 4)
			cooldown.replace(uuid, new Cooldown(values.getCombo() + 1));
		else
			cooldown.replace(uuid, new Cooldown(values.getCombo()));
		
		event.setDamage(event.getDamage() * values.getCombo());
		damager.sendMessage(Utils.color("&6&lx&a&l" + cooldown.get(damager.getUniqueId()).getCombo() + " &e&lDamage Dealt!"));
	}
	
	public static class Cooldown {
		private final long cooldown;
		private final int combo;
		
		Cooldown(int combo)
		{
			this.cooldown = System.currentTimeMillis();
			this.combo = combo;
		}
		
		public Cooldown()
		{
			this.cooldown = 0L;
			this.combo = 1;
		}
		
		long getCooldown()
		{
			return cooldown;
		}
		
		int getCombo()
		{
			return combo;
		}
	}
}
