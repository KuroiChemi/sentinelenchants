package me.chemi.Enchantments.Bow;

import me.chemi.CEMain;
import me.chemi.Enchantments.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class Arrow
		extends CEnchantment {
	
	public Arrow()
	{
		super(Application.BOW, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.BOW_SHOOT);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		EntityShootBowEvent event = (EntityShootBowEvent) e;
		Player p = (Player) event.getEntity();
		
		if (event.getForce() > 0.6)
		{
			event.setCancelled(true);
			new BukkitRunnable() {
				int count = 1;
				
				@Override
				public void run()
				{
					p.launchProjectile(org.bukkit.entity.Arrow.class, event.getProjectile().getVelocity());
					
					if (++count > level * 2)
						cancel();
				}
			}.runTaskTimer(CEMain.plugin, 0L, 3L);
		}
	}
}
