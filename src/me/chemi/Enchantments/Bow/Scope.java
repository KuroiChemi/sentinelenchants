package me.chemi.Enchantments.Bow;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

public class Scope
		extends CEnchantment {
	
	public Scope()
	{
		super(Application.BOW, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.DAMAGE_GIVEN);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		
		if (!(event.getDamager() instanceof org.bukkit.entity.Arrow)) return;
		
		double extraDamage = event.getDamage() * ((level + 1) * 10) / 100;
		event.setDamage(event.getDamage() + extraDamage);
	}
}
