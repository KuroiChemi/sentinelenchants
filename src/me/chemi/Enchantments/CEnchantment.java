package me.chemi.Enchantments;

import me.chemi.CEBasic;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

public abstract class CEnchantment
		extends CEBasic {
	
	private final Application app;
	private final List<Tier> tiers;
	
	protected CEnchantment(Application app, Tier... tier)
	{
		this.app = app;
		this.tiers = Arrays.asList(tier);
		
		this.displayName = getClass().getSimpleName();
		char[] nameChars = this.displayName.toCharArray();
		for (int i = 3; i < nameChars.length; i++)
		{
			if (Character.isUpperCase(nameChars[i]))
			{
				this.displayName = (this.displayName.substring(0, i) + " " + this.displayName.substring(i, nameChars.length));
			}
		}
	}
	
	public Application getApplication()
	{
		return this.app;
	}
	
	public List<Tier> getTiers()
	{
		return this.tiers;
	}
	
	public int getEnchantmentMaxLevel()
	{
		return EnchantManager.levelToInt(this.tiers.get(this.tiers.size() - 1).toString());
	}
	
	protected boolean generateChance(double percentage)
	{
		double chance = Math.random();
		return chance < (percentage / 100);
	}
	
	public abstract void effect(Event paramEvent, ItemStack paramItemStack, int paramInt);
	
	public enum Application {
		BOOTS, HELMET, CHEST, LEGGINGS, PICKAXE, ARMOR, WEAPON, BOW, SWORD
	}
	
	public enum Tier {
		I, II, IIII, III
	}
}
