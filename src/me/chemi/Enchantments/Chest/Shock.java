package me.chemi.Enchantments.Chest;

import me.chemi.CEMain;
import me.chemi.Enchantments.CEnchantment;
import me.chemi.Utils.Utils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class Shock
		extends CEnchantment {
	
	public Shock()
	{
		super(Application.CHEST, Tier.IIII);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		LivingEntity damager = Utils.getDamager((EntityDamageByEntityEvent) e);
		
		if (generateChance(5))
		{
			damager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 120, 50), true);
			
			new BukkitRunnable() {
				int count = 3;
				
				@Override
				public void run()
				{
					if (count <= 0)
						cancel();
					count--;
					damager.getWorld().strikeLightning(damager.getLocation());
				}
			}.runTaskTimer(CEMain.plugin, 5L, 10L);
		}
	}
}
