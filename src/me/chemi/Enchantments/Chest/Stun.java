package me.chemi.Enchantments.Chest;

import me.chemi.Enchantments.CEnchantment;
import me.chemi.Utils.Utils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Stun
		extends CEnchantment {
	
	public Stun()
	{
		super(Application.CHEST, Tier.III);
		this.triggers.add(Trigger.DAMAGE_TAKEN);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		LivingEntity damager = Utils.getDamager((EntityDamageByEntityEvent) e);
		
		if (generateChance(5))
			damager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 40 * level, 10), true);
	}
}
