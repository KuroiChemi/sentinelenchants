package me.chemi.Enchantments.Chest;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class Insanity
		extends CEnchantment {
	
	public Insanity()
	{
		super(Application.CHEST, Tier.II, Tier.III);
		this.triggers.add(Trigger.WEAR_ITEM);
		this.potionsOnWear.put(PotionEffectType.INCREASE_DAMAGE, -1);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
	}
	
}
