package me.chemi.Enchantments.Sword;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

public class Grind
		extends CEnchantment {
	
	public Grind()
	{
		super(Application.SWORD, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.DEATH);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDeathEvent event = (EntityDeathEvent) e;
		int max = (level == 1) ? 13 : 13 + level;
		
		event.setDroppedExp(Math.min((event.getDroppedExp() * level + 1), max));
	}
}

