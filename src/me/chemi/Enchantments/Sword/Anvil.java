package me.chemi.Enchantments.Sword;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Anvil
		extends CEnchantment {
	
	public static List<UUID> anvilList = new ArrayList<>();
	
	public Anvil()
	{
		super(Application.SWORD, Tier.III);
		this.triggers.add(Trigger.DAMAGE_GIVEN);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		
		if (generateChance(30 * level))
		{
			Player target = (Player) event.getEntity();
			Block b = target.getWorld().getBlockAt(target.getLocation().add(0, 2, 0));
			
			if (b.getType() == Material.AIR)
			{
				target.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 80, 50));
				b.setType(Material.ANVIL);
			}
		}
	}
}