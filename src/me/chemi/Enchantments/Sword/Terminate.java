package me.chemi.Enchantments.Sword;

import me.chemi.CEMain;
import me.chemi.CEventHandler;
import me.chemi.Enchantments.CEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Terminate
		extends CEnchantment {
	
	public static final List<UUID> terminated = new ArrayList<>();
	
	public Terminate()
	{
		super(Application.SWORD, Tier.I, Tier.II, Tier.III, Tier.IIII);
		this.triggers.add(Trigger.DAMAGE_GIVEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		
		if (!(event.getEntity() instanceof Player)) return;
		
		Player p = (Player) event.getEntity();
		
		if (terminated.contains(p.getUniqueId())) return;
		
		int chance = (level > 2) ? 5 * level : 4 + (4 * level);
		
		if (generateChance(chance))
		{
			for (ItemStack piece : p.getInventory().getArmorContents())
				CEventHandler.handleArmor(p, piece, true);
			
			terminated.add(p.getUniqueId());
			
			new BukkitRunnable() {
				@Override
				public void run()
				{
					terminated.remove(p.getUniqueId());
					
					if (p.isOnline() && !p.isDead())
					{
						for (ItemStack piece : p.getInventory().getArmorContents())
							CEventHandler.handleArmor(p, piece, false);
					}
				}
			}.runTaskLater(CEMain.plugin, 100L);
		}
	}
}
