package me.chemi.Enchantments.Sword;

import me.chemi.Enchantments.CEnchantment;
import me.chemi.Utils.Utils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

public class Vampire
		extends CEnchantment {
	
	public Vampire()
	{
		super(Application.SWORD, Tier.I, Tier.II, Tier.III);
		this.triggers.add(Trigger.DAMAGE_GIVEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		LivingEntity damager = Utils.getDamager((EntityDamageByEntityEvent) e);
		
		double heal = damager.getHealth() + 2;
		
		if (heal < damager.getHealth())
		{
			if ((level < 3 && generateChance(5 * level)) || (level == 3 && generateChance(25)))
			{
				damager.setHealth(heal);
			}
		}
	}
}