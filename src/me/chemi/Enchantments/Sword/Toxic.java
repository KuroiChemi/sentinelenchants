package me.chemi.Enchantments.Sword;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Toxic
		extends CEnchantment {
	
	public Toxic()
	{
		super(Application.SWORD, Tier.I, Tier.II);
		this.triggers.add(Trigger.DAMAGE_GIVEN);
	}
	
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		LivingEntity target = (LivingEntity) event.getEntity();
		if (generateChance(5))
			target.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 5, level), true);
		
	}
}
