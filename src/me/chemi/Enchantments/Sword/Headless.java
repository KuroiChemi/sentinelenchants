package me.chemi.Enchantments.Sword;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class Headless
		extends CEnchantment {
	
	public Headless()
	{
		super(Application.SWORD, Tier.III);
		this.triggers.add(Trigger.DEATH);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDeathEvent event = (EntityDeathEvent) e;
		
		boolean chance = false;
		
		switch (level)
		{
			case 1:
				chance = generateChance(50);
				break;
			case 2:
				chance = generateChance(60);
				break;
			case 3:
				chance = generateChance(90);
		}
		
		if (chance)
		{
			ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1);
			SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
			skullMeta.setOwner(event.getEntity().getName());
			skull.setItemMeta(skullMeta);
			
			event.getEntity().getWorld().dropItem(event.getEntity().getLocation(), skull);
		}
	}
}