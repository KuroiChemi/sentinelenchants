package me.chemi.Enchantments.Sword;

import me.chemi.Enchantments.CEnchantment;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

public class Demolish
		extends CEnchantment {
	
	public Demolish()
	{
		super(Application.SWORD, Tier.I, Tier.II, Tier.III, Tier.IIII);
		this.triggers.add(Trigger.DAMAGE_GIVEN);
	}
	
	@Override
	public void effect(Event e, ItemStack item, int level)
	{
		EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
		
		if (generateChance(10 * level))
			event.setDamage(event.getDamage() * 2);
	}
}
