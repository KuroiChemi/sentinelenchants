package me.chemi.Utils;

import me.chemi.CEMain;
import me.chemi.Enchantments.CEnchantment;
import me.chemi.Enchantments.CEnchantment.Tier;
import me.chemi.Enchantments.EnchantManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

class ScrollEffect {
	
	final Inventory GUI;
	private final List<ItemStack> prizeList = new ArrayList<>();
	private final Player p;
	private final Plugin plugin;
	private int lastRandom = 1;
	private BukkitTask randomPrizeTask;
	private BukkitTask randomPrizeTask2;
	private BukkitTask randomPrizeTask3;
	private BukkitTask randomPrizeTask4;
	private BukkitTask randomPrizeTask5;
	private BukkitTask randomGlassTask;
	
	ScrollEffect(Player p, Tier tier)
	{
		this.plugin = CEMain.plugin;
		this.p = p;
		GUI = Bukkit.createInventory(p, 27, ChatColor.translateAlternateColorCodes('&', "&9Book &eShrine"));
		
		List<CEnchantment> cList = EnchantManager.getTierEnchantments(tier);
		
		prizeList.addAll(cList.stream().map(ce -> EnchantManager.getEnchantBook(ce, ce.getTiers().indexOf(tier) + 1)).collect(Collectors.toList()));
		
		p.openInventory(GUI);
		run();
	}
	
	private void run()
	{
		randomGlassTask = new BukkitRunnable() {
			public void run()
			{
				whileStatement();
			}
		}.runTaskTimer(plugin, 0L, 4L);
		
		randomPrizeTask = new BukkitRunnable() {
			public void run()
			{
				lastRandomEquals();
			}
		}.runTaskTimer(plugin, 0L, 10L);
		
		randomPrizeTask2 = new BukkitRunnable() {
			public void run()
			{
				randomPrizeTask.cancel();
				lastRandomEquals();
			}
		}.runTaskTimer(plugin, 40L, 8L);
		
		randomPrizeTask3 = new BukkitRunnable() {
			public void run()
			{
				randomPrizeTask2.cancel();
				lastRandomEquals();
			}
		}.runTaskTimer(plugin, 60L, 6L);
		
		randomPrizeTask4 = new BukkitRunnable() {
			public void run()
			{
				randomPrizeTask3.cancel();
				lastRandomEquals();
			}
		}.runTaskTimer(plugin, 80L, 4L);
		
		randomPrizeTask5 = new BukkitRunnable() {
			public void run()
			{
				randomPrizeTask4.cancel();
				lastRandomEquals();
			}
		}.runTaskTimer(plugin, 100L, 2L);
		
		new BukkitRunnable() {
			public void run()
			{
				randomPrizeTask5.cancel();
				randomGlassTask.cancel();
				
				new BukkitRunnable() {
					public void run()
					{
						if (p.getInventory().firstEmpty() == -1)
							p.getWorld().dropItemNaturally(p.getLocation(), GUI.getItem(13));
						else
							p.getInventory().addItem(GUI.getItem(13));
						
						if (GUI.getViewers().contains(p))
							p.closeInventory();
						
						GUI.clear();
						CEMain.plugin.getRandomGUI().remove(p.getUniqueId());
					}
				}.runTaskLater(plugin, 40L);
			}
		}.runTaskLater(plugin, 160L);
	}
	
	private void whileStatement()
	{
		int x = 0;
		while (x != 27)
		{
			if (x == 13)
			{
				x++;
			}
			else
			{
				ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1);
				ItemMeta glassMeta = glass.getItemMeta();
				glassMeta.setDisplayName(ChatColor.YELLOW.toString());
				glass.setItemMeta(glassMeta);
				glass.setDurability((short) new Random().nextInt(15));
				GUI.setItem(x, glass);
				x++;
			}
		}
	}
	
	private void lastRandomEquals()
	{
		if (this.lastRandom == -1)
		{
			this.lastRandom = new Random().nextInt(this.prizeList.size());
			this.GUI.setItem(13, (this.prizeList.get(this.lastRandom)));
		}
		else
		{
			int newRandom = this.lastRandom;
			while (this.lastRandom == newRandom)
			{
				newRandom = new Random().nextInt(prizeList.size());
			}
			this.GUI.setItem(13, this.prizeList.get(newRandom));
			this.lastRandom = newRandom;
		}
		p.updateInventory();
	}
}
