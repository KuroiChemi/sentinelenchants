package me.chemi.Utils;

import me.chemi.CEMain;
import me.chemi.Enchantments.CEnchantment;
import me.chemi.Enchantments.CEnchantment.Application;
import me.chemi.Enchantments.CEnchantment.Tier;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collections;
import java.util.Set;

public class Utils {
	
	private static Inventory MainMenu;
	private static Inventory EnchantMenu;
	
	public static Inventory getMagicAnvil()
	{
		return Bukkit.createInventory(null, InventoryType.FURNACE, color("&9Magic &eAnvil"));
	}
	
	public static String color(String str)
	{
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	public static Inventory getMainMenu()
	{
		return MainMenu;
	}
	
	public static Inventory getEnchantMenu()
	{
		return EnchantMenu;
	}
	
	public static boolean inPlace(int slot, String mat)
	{
		return mat.endsWith("HELMET") && slot == 5 || mat.endsWith("CHESTPLATE") && slot == 6 || mat.endsWith("LEGGINGS") && slot == 7 || mat.endsWith("BOOTS") && slot == 8;
	}
	
	public static boolean inPlace(Set<Integer> slots, String mat)
	{
		return mat.endsWith("HELMET") && slots.contains(5) || mat.endsWith("CHESTPLATE") && slots.contains(6) || mat.endsWith("LEGGINGS") && slots.contains(7) || mat.endsWith("BOOTS") && slots.contains(8);
	}
	
	public static boolean isApplicable(ItemStack i, CEnchantment ce)
	{
		String mat = i.getType().toString();
		
		return
				(ce.getApplication() == Application.PICKAXE && mat.endsWith("PICKAXE")) ||
						(ce.getApplication() == Application.HELMET && mat.endsWith("HELMET")) ||
						(ce.getApplication() == Application.CHEST && mat.endsWith("CHESTPLATE")) ||
						(ce.getApplication() == Application.LEGGINGS && mat.endsWith("LEGGINGS")) ||
						(ce.getApplication() == Application.BOOTS && mat.endsWith("BOOTS")) ||
						(ce.getApplication() == Application.SWORD && mat.endsWith("SWORD")) ||
						(ce.getApplication() == Application.BOW && mat.equals("BOW")) ||
						(ce.getApplication() == Application.ARMOR && isArmor(mat)) ||
						(ce.getApplication() == Application.WEAPON && isWeapon(mat));
	}
	
	public static boolean isArmor(String mat)
	{
		return mat.endsWith("HELMET") || mat.endsWith("CHESTPLATE") || mat.endsWith("LEGGINGS") || mat.endsWith("BOOTS");
	}
	
	private static boolean isWeapon(String mat)
	{
		return mat.endsWith("SWORD") || mat.endsWith("AXE") || mat.equals("BOW");
	}
	
	public static boolean isUsable(String bMat)
	{
		return ((bMat.contains("ORE")) || ((!bMat.contains("STAIRS")) && (bMat.contains("STONE"))) || (bMat.equals("STAINED_CLAY")) || (bMat.equals("NETHERRACK")));
	}
	
	public static void generateInventories()
	{
		ItemStack backButton = new ItemStack(Material.NETHER_STAR);
		
		ItemMeta tempMeta = backButton.getItemMeta();
		String tempLore;
		
		tempMeta.setDisplayName(color("&9&lBack"));
		backButton.setItemMeta(tempMeta);
		
		// MAIN MENU
		Inventory mainMenu = Bukkit.createInventory(null, 9, color("&9Sentinel &eEnchants"));
		ItemStack enchantments = new ItemStack(Material.ENCHANTMENT_TABLE);
		ItemStack magicAnvil = new ItemStack(Material.ANVIL);
		
		tempMeta.setDisplayName((color("&9&lEnchantments")));
		tempLore = color("&7A black portal opens.");
		tempMeta.setLore(Collections.singletonList(tempLore));
		
		enchantments.setItemMeta(tempMeta);
		
		tempMeta.setDisplayName(color("&9&lMagic Anvil"));
		tempLore = color("&7The magical anvil glows in bright colors.");
		tempMeta.setLore(Collections.singletonList(tempLore));
		
		magicAnvil.setItemMeta(tempMeta);
		
		mainMenu.setItem(2, enchantments);
		mainMenu.setItem(6, magicAnvil);
		// MAIN MENU
		
		// TIER MENU
		Inventory enchantMenu = Bukkit.createInventory(null, 27, color("&9Enchantment &eMenu"));
		enchantMenu.setItem(26, backButton);
		ItemStack tier = new ItemStack(Material.STAINED_GLASS_PANE);
		
		tempMeta.setDisplayName(color("&c&lBronze"));
		tempLore = color("&430 &elevels");
		tempMeta.setLore(Collections.singletonList(tempLore));
		tier.setItemMeta(tempMeta);
		tier.setDurability((short) 12);
		enchantMenu.setItem(10, tier);
		
		tempMeta.setDisplayName(color("&7&lSilver"));
		tempLore = color("&850 &elevels");
		tempMeta.setLore(Collections.singletonList(tempLore));
		tier.setItemMeta(tempMeta);
		tier.setDurability((short) 7);
		enchantMenu.setItem(12, tier);
		
		tempMeta.setDisplayName(color("&e&lGold"));
		tempLore = color("&670 &elevels");
		tempMeta.setLore(Collections.singletonList(tempLore));
		tier.setItemMeta(tempMeta);
		tier.setDurability((short) 1);
		enchantMenu.setItem(14, tier);
		
		tempMeta.setDisplayName(color("&b&lPlatinum"));
		tempLore = color("&390 &elevels");
		tempMeta.setLore(Collections.singletonList(tempLore));
		tier.setItemMeta(tempMeta);
		tier.setDurability((short) 3);
		enchantMenu.setItem(16, tier);
		// TIER MENU
		
		MainMenu = mainMenu;
		EnchantMenu = enchantMenu;
	}
	
	public static void spawnRandomBook(Player p, Tier tier)
	{
		CEMain.plugin.getRandomGUI().put(p.getUniqueId(), new ScrollEffect(p, tier).GUI);
	}
	
	public static LivingEntity getDamager(EntityDamageByEntityEvent e)
	{
		if (e.getDamager() instanceof LivingEntity)
			return (LivingEntity) e.getDamager();
		else if (e.getDamager() instanceof Projectile)
			return (LivingEntity) ((Projectile) e.getDamager()).getShooter();
		else
			return (LivingEntity) e.getEntity();
	}
	
}
