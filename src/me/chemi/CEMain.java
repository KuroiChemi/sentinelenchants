package me.chemi;

import me.chemi.Enchantments.CEnchantment;
import me.chemi.Enchantments.EnchantManager;
import me.chemi.Utils.Utils;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class CEMain
		extends JavaPlugin {
	
	public static CEMain plugin;
	private Map<UUID, Inventory> randomGUI;
	private String prefix;
	
	@Override
	public void onEnable()
	{
		plugin = this;
		prefix = Utils.color("&9[&6Sentinel&eEnchants&9] ");
		randomGUI = new HashMap<>();
		
		makeLists();
		Utils.generateInventories();
		
		getCommand("se").setExecutor(new CECommand());
		getServer().getPluginManager().registerEvents(new CEListener(), this);
	}
	
	private void makeLists()
	{
		try
		{
			String path = getDataFolder().getAbsolutePath();
			path = path + ".jar";
			JarFile jar = new JarFile(path);
			Enumeration<JarEntry> entries = jar.entries();
			
			while (entries.hasMoreElements())
			{
				String entryName = entries.nextElement().getName();
				if (!entryName.contains("$") && entryName.contains("Enchantments") && entryName.endsWith(".class")
						    && !(entryName.contains("CEnchantment") || entryName.contains("EnchantManager") || entryName.contains("GlowEnchantment")))
				{
					String className = entryName.replace(".class", "");
					
					if (entryName.contains("/"))
						className = className.replaceAll("/", ".");
					else if (entryName.contains("\\"))
						className = className.replaceAll("\\\\", ".");
					
					EnchantManager.getEnchantments().add((CEnchantment) getClassLoader().loadClass(className).getDeclaredConstructor().newInstance());
				}
			}
			
			jar.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			getServer().getPluginManager().disablePlugin(this);
		}
	}
	
	public Map<UUID, Inventory> getRandomGUI()
	{
		return randomGUI;
	}
	
	String getPrefix()
	{
		return prefix;
	}
}
