package me.chemi;

import me.chemi.Enchantments.CEnchantment;
import me.chemi.Enchantments.EnchantManager;
import me.chemi.Utils.Utils;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

class CECommand
		implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (!(sender instanceof Player))
			return true;
		
		Player p = (Player) sender;
		
		if (args.length != 1 && args.length != 2)
		{
			if (CEMain.plugin.getRandomGUI().containsKey(p.getUniqueId()))
				p.openInventory(CEMain.plugin.getRandomGUI().get(p.getUniqueId()));
			else
				p.openInventory(Utils.getMainMenu());
			
			p.updateInventory();
		}
		else if (args.length == 1)
		{
			String book = args[0];
			
			CEnchantment exists = null;
			for (CEnchantment ce : EnchantManager.getEnchantments())
			{
				if (ChatColor.stripColor(ce.getDisplayName()).equalsIgnoreCase(book))
					exists = ce;
			}
			
			if (exists != null)
			{
				p.sendMessage(CEMain.plugin.getPrefix() + Utils.color("&aYou've now got the ") + Utils.color("&6") + exists.getDisplayName() + Utils.color(" &b&lMagic Book&a!"));
				p.getInventory().addItem(EnchantManager.getEnchantBook(exists, 1));
			}
			else
			{
				p.sendMessage(CEMain.plugin.getPrefix() + Utils.color("&6") + book + Utils.color(" &cdoes not exist!"));
			}
		}
		else
		{
			String book = args[0];
			int level = Integer.parseInt(args[1]);
			
			CEnchantment exists = null;
			
			for (CEnchantment ce : EnchantManager.getEnchantments())
			{
				if (ChatColor.stripColor(ce.getDisplayName()).equalsIgnoreCase(book))
					exists = ce;
			}
			
			if (exists != null && EnchantManager.isViable(exists, level))
			{
				p.sendMessage(CEMain.plugin.getPrefix() + Utils.color("&aYou've now got the ") + Utils.color("&6") + exists.getDisplayName() + Utils.color(" &b&lMagic Book&a!"));
				p.getInventory().addItem(EnchantManager.getEnchantBook(exists, level));
			}
			else if (exists == null)
			{
				p.sendMessage(CEMain.plugin.getPrefix() + Utils.color("&6") + book + Utils.color(" &cdoes not exist!"));
			}
			else
			{
				p.sendMessage(CEMain.plugin.getPrefix() + Utils.color("&6") + level + Utils.color(" &cis not viable level for this book!"));
			}
		}
		return true;
	}
	
}
