package me.chemi;

import com.google.common.collect.Sets;
import me.chemi.CEBasic.Trigger;
import me.chemi.Enchantments.Armor.Hardened;
import me.chemi.Enchantments.Boots.Swarm;
import me.chemi.Enchantments.Boots.Wings;
import me.chemi.Enchantments.CEnchantment;
import me.chemi.Enchantments.CEnchantment.Tier;
import me.chemi.Enchantments.EnchantManager;
import me.chemi.Enchantments.Helmet.Feast;
import me.chemi.Enchantments.Sword.Grind;
import me.chemi.Enchantments.Sword.Headless;
import me.chemi.Enchantments.Weapons.Combo;
import me.chemi.Enchantments.Weapons.Combo.Cooldown;
import me.chemi.Utils.Utils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import static me.chemi.Enchantments.Sword.Anvil.anvilList;

class CEListener
		implements Listener {
	
	private final HashSet<CEBasic> damageTaken = new HashSet<>();
	private final HashSet<CEBasic> damageGiven = new HashSet<>();
	private final HashSet<CEBasic> blockBroken = new HashSet<>();
	
	CEListener()
	{
		for (CEnchantment ce : EnchantManager.getEnchantments())
		{
			if (ce.getTriggers().contains(Trigger.DAMAGE_TAKEN))
				damageTaken.add(ce);
			if (ce.getTriggers().contains(CEBasic.Trigger.BLOCK_BROKEN))
				blockBroken.add(ce);
			if (ce.getTriggers().contains(CEBasic.Trigger.DAMAGE_GIVEN))
				damageGiven.add(ce);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void inventoryMenuPrevention(InventoryDragEvent e)
	{
		
		String invName = ChatColor.stripColor(e.getView().getTopInventory().getTitle());
		
		if (invName.equals("Sentinel Enchants") || invName.equals("Enchantment Menu") || invName.equals("Book Shrine"))
			e.setCancelled(true);
		else if (invName.equals("Magic Anvil"))
			CEventHandler.updateEnchantingInventory(e.getInventory(), (Player) e.getWhoClicked());
		else if (e.getView().getTopInventory().getType() == InventoryType.CRAFTING || e.getView().getTopInventory().getType() == InventoryType.PLAYER)
			CEventHandler.handleArmor((Player) e.getWhoClicked(), e.getOldCursor(), false);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void EntityDamageByEntityEvent(EntityDamageByEntityEvent e)
	{
		Entity damager = e.getDamager();
		Entity damaged = e.getEntity();
		
		if (damager instanceof FallingBlock)
		{
			FallingBlock f = (FallingBlock) damager;
			if (f.getMaterial() == Material.ANVIL)
				anvilList.add(f.getUniqueId());
		}
		
		if (damager.getUniqueId().equals(damaged.getUniqueId()))
			return;
		
		if (damager instanceof Wolf && damaged instanceof LivingEntity)
		{
			if (damager.hasMetadata("extraDamage"))
				e.setDamage(e.getDamage() * damager.getMetadata("extraDamage").get(0).asInt());
		}
		
		if (damager instanceof Player)
		{
			CEventHandler.handleEvent((Player) damager, e, this.damageGiven);
			
			if (damaged instanceof Wolf && damaged.hasMetadata("owner"))
			{
				if (damaged.getMetadata("owner").get(0).asString().equals(damager.getUniqueId().toString()))
					e.setCancelled(true);
			}
		}
		else if (damager instanceof Projectile && ((Projectile) damager).getShooter() instanceof Player)
		{
			CEventHandler.handleEvent((Player) ((Projectile) damager).getShooter(), e, this.damageGiven);
		}
		
		if (damaged instanceof Player)
		{
			if (Combo.cooldown.containsKey(damaged.getUniqueId()))
			{
				Combo.cooldown.replace(damaged.getUniqueId(), new Cooldown());
			}
			
			if (Swarm.spawned.containsKey(damaged.getUniqueId()))
			{
				if (Swarm.spawned.get(damaged.getUniqueId()).getMinions().contains(damager.getUniqueId()))
				{
					e.setCancelled(true);
					damager.remove();
				}
			}
			
			CEventHandler.handleEvent((Player) damaged, e, this.damageTaken);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void EntityTargetLivingEntityEvent(EntityTargetLivingEntityEvent e)
	{
		if (!(e.getEntity() instanceof Zombie) || !(e.getTarget() instanceof Player)) return;
		
		if (Swarm.spawned.containsKey(e.getTarget().getUniqueId()))
		{
			List<UUID> zombies = Swarm.spawned.get(e.getTarget().getUniqueId()).getMinions();
			
			if (zombies.contains(e.getEntity().getUniqueId()))
			{
				Entity ent = Swarm.spawned.get(e.getTarget().getUniqueId()).getAttacked();
				if (ent.isDead())
				{
					e.getTarget().getNearbyEntities(30, 30, 30).stream().filter(z -> z instanceof Zombie && zombies.contains(z.getUniqueId())).forEach(Entity::remove);
					Swarm.spawned.remove(e.getTarget().getUniqueId());
				}
				else
					e.setTarget(ent);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void EntityBlockFormEvent(EntityChangeBlockEvent e)
	{
		if (anvilList.contains(e.getEntity().getUniqueId()))
		{
			e.setCancelled(true);
			e.getBlock().setType(Material.AIR);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerItemBreakEvent(PlayerItemBreakEvent e)
	{
		ItemStack item = e.getBrokenItem();
		if (Utils.isArmor(item.getType().toString()))
		{
			CEventHandler.handleArmor(e.getPlayer(), item, true);
			CEventHandler.handleEvent(e.getPlayer(), e, Sets.newHashSet(new Hardened()));
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void InventoryClickEvent(InventoryClickEvent e)
	{
		if (e.getSlot() == -999 || e.getSlot() == -1)
			return;
		
		Inventory topInv = e.getView().getTopInventory();
		String invName = ChatColor.stripColor(topInv.getTitle());
		
		if (invName.equals("Magic Anvil"))
		{
			CEventHandler.handleEnchanting(e);
			return;
		}
		
		if (topInv.getType() == InventoryType.CRAFTING || topInv.getType() == InventoryType.PLAYER)
		{
			if (e.getSlotType() == InventoryType.SlotType.ARMOR && e.getClick() != ClickType.DOUBLE_CLICK)
			{
				CEventHandler.handleArmor((Player) e.getWhoClicked(), e.getCurrentItem(), true);
				
				if (Utils.inPlace(e.getRawSlot(), e.getCursor().getType().toString()))
					CEventHandler.handleArmor((Player) e.getWhoClicked(), e.getCursor(), false);
				
				if (e.getCursor() == null)
					CEventHandler.handleArmor((Player) e.getWhoClicked(), e.getCurrentItem(), false);
				
			}
			else if (e.getClick() == ClickType.SHIFT_LEFT)
			{
				ItemStack current = e.getCurrentItem();
				String typeS = current.getType().toString();
				PlayerInventory inv = e.getWhoClicked().getInventory();
				if ((typeS.endsWith("HELMET") && inv.getHelmet() == null) || (typeS.endsWith("CHESTPLATE") && inv.getChestplate() == null) || (typeS.endsWith("LEGGINGS") && inv.getLeggings() == null)
						    || (typeS.endsWith("BOOTS") && inv.getBoots() == null))
					CEventHandler.handleArmor((Player) e.getWhoClicked(), e.getCurrentItem(), false);
			}
		}
		
		if (e.getCurrentItem() == null || e.getCurrentItem().getType().equals(Material.AIR))
			return;
		
		final Player p = (Player) e.getWhoClicked();
		
		switch (invName)
		{
			case "Sentinel Enchants":
				if (e.getRawSlot() == 2)
				{
					p.closeInventory();
					p.openInventory(Utils.getEnchantMenu());
				}
				else if (e.getRawSlot() == 6)
				{
					p.closeInventory();
					p.openInventory(Utils.getMagicAnvil());
				}
				break;
			
			case "Enchantment Menu":
				int levels = p.getLevel();
				
				if (e.getRawSlot() == 10 && levels >= 30)
				{
					Utils.spawnRandomBook(p, Tier.I);
					p.setLevel(levels - 30);
				}
				else if (e.getRawSlot() == 12 && levels >= 50)
				{
					Utils.spawnRandomBook(p, Tier.II);
					p.setLevel(levels - 50);
				}
				else if (e.getRawSlot() == 14 && levels >= 70)
				{
					Utils.spawnRandomBook(p, Tier.III);
					p.setLevel(levels - 70);
				}
				else if (e.getRawSlot() == 16 && levels >= 90)
				{
					Utils.spawnRandomBook(p, Tier.IIII);
					p.setLevel(levels - 90);
				}
				else if ((e.getRawSlot() == topInv.getSize() - 1))
				{
					p.closeInventory();
					p.openInventory(Utils.getMainMenu());
				}
				else if (e.getRawSlot() > 9 && e.getRawSlot() < 17)
				{
					p.closeInventory();
					p.sendMessage(CEMain.plugin.getPrefix() + Utils.color("&cYou do not have enough &a&lXP &cto buy this enchantment."));
					return;
				}
				break;
			
			case "Book Shrine":
				break;
			
			default:
				return;
		}
		
		e.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void inventoryClose(InventoryCloseEvent event)
	{
		if (ChatColor.stripColor(event.getInventory().getName())
				    .equals("Magic Anvil"))
		{
			ItemStack[] contents = event.getInventory().getContents();
			HumanEntity p = event.getPlayer();
			Location loc = p.getLocation().add(0, 1.25, 0);
			Vector velocity = loc.getDirection().multiply(0.25);
			if (contents[0] != null && !contents[0].getType().equals(Material.AIR))
				if (p.getInventory().firstEmpty() == -1)
					p.getWorld().dropItem(loc, contents[0]).setVelocity(velocity);
				else
					p.getInventory().addItem(contents[0]);
			if (contents[1] != null && !contents[1].getType().equals(Material.AIR))
				if (p.getInventory().firstEmpty() == -1)
					p.getWorld().dropItem(loc, contents[1]).setVelocity(velocity);
				else
					p.getInventory().addItem(contents[1]);
		}
		((Player) event.getPlayer()).updateInventory();
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerInteractEvent(PlayerInteractEvent e)
	{
		Player p = e.getPlayer();
		
		if (p.getItemInHand().getType() == Material.AIR)
			return;
		
		if (e.getAction() != Action.RIGHT_CLICK_AIR && e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;
		
		ItemStack i = p.getItemInHand();
		String mat = i.getType().toString();
		PlayerInventory inv = p.getInventory();
		if ((mat.endsWith("BOOTS") && inv.getBoots() == null) || (mat.endsWith("LEGGINGS") && inv.getLeggings() == null) || (mat.endsWith("CHESTPLATE") && inv.getChestplate() == null)
				    || (mat.endsWith("HELMET") && inv.getHelmet() == null))
			CEventHandler.handleArmor(p, e.getItem(), false);
	}
	
	@EventHandler
	public void PlayerDeathEvent(EntityDeathEvent e)
	{
		Player p = e.getEntity().getKiller();
		
		if (p != null)
		{
			CEventHandler.handleEvent(p, e, Sets.newHashSet(new Grind()));
			
			if (e.getEntity() instanceof Player)
				CEventHandler.handleEvent(p, e, Sets.newHashSet(new Headless()));
		}
		
		if (e.getEntity() instanceof Player)
			CEventHandler.handleArmor((Player) e.getEntity(), ((Player) e.getEntity()).getInventory().getHelmet(), true);
	}
	
	@EventHandler
	public void ProjectileLaunchEvent(EntityShootBowEvent e)
	{
		if (e.getEntity() instanceof Player)
			CEventHandler.handleEvent((Player) e.getEntity(), e, Sets.newHashSet(new me.chemi.Enchantments.Bow.Arrow()));
	}
	
	@EventHandler
	public void PlayerRespawnEvent(PlayerRespawnEvent e)
	{
		Player p = e.getPlayer();
		
		if (p.getInventory().getHelmet() != null)
			CEventHandler.handleArmor(p, p.getInventory().getHelmet(), true);
		
		if (p.getInventory().getChestplate() != null)
			CEventHandler.handleArmor(p, p.getInventory().getChestplate(), true);
		
		if (p.getInventory().getLeggings() != null)
			CEventHandler.handleArmor(p, p.getInventory().getLeggings(), true);
		
		if (p.getInventory().getBoots() != null)
			CEventHandler.handleArmor(p, p.getInventory().getBoots(), true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void FoodLevelChangeEvent(FoodLevelChangeEvent e)
	{
		if (e.getEntity() instanceof Player)
		{
			Player p = (Player) e.getEntity();
			CEventHandler.handleEvent(p, e, Sets.newHashSet(new Feast()));
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void BlockBreakEvent(BlockBreakEvent e)
	{
		CEventHandler.handleEvent(e.getPlayer(), e, this.blockBroken);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerQuitEvent(PlayerQuitEvent e)
	{
		UUID uuid = e.getPlayer().getUniqueId();
		if (Combo.cooldown.containsKey(uuid))
			Combo.cooldown.remove(uuid);
		
		if (Swarm.spawned.containsKey(uuid))
			Swarm.spawned.remove(uuid);
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void PlayerMoveEvent(PlayerMoveEvent e)
	{
		Location from = e.getFrom();
		Location to = e.getTo();
		
		if (from.getBlockX() != to.getBlockX() || from.getBlockY() != to.getBlockY() || from.getBlockZ() != to.getBlockZ())
			CEventHandler.handleEvent(e.getPlayer(), e, Sets.newHashSet(new Wings()));
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void PlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent e)
	{
		Player p = e.getPlayer();
		String cmd = e.getMessage().split(" ")[0];
		
		if (p.hasPermission("essentials.hat") && cmd.equalsIgnoreCase("/hat"))
		{
			CEventHandler.handleArmor(p, p.getInventory().getHelmet(), true);
		}
	}
}
