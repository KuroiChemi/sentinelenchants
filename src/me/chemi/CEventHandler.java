package me.chemi;

import me.chemi.Enchantments.CEnchantment;
import me.chemi.Enchantments.Chest.Insanity;
import me.chemi.Enchantments.EnchantManager;
import me.chemi.Enchantments.Helmet.Feast;
import me.chemi.Enchantments.Sword.Terminate;
import me.chemi.Utils.Utils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

public class CEventHandler {
	
	public static void handleArmor(Player toCheck, ItemStack toAdd, Boolean remove)
	{
		if (Terminate.terminated.contains(toCheck.getUniqueId()))
			return;
		
		if (toAdd == null || toAdd.getType() == Material.AIR || !toAdd.hasItemMeta() || !toAdd.getItemMeta().hasLore())
			return;
		
		if (remove)
		{
			List<PotionEffectType> potionsToRemove = new ArrayList<>();
			
			for (String line : toAdd.getItemMeta().getLore())
			{
				CEnchantment ce = EnchantManager.getEnchantment(line);
				
				if (ce == null) continue;
				
				potionsToRemove.addAll(ce.getPotionEffectsOnWear().keySet());
			}
			
			for (ItemStack item : toCheck.getInventory().getArmorContents())
			{
				if (item == null || item.getType() == Material.AIR || !item.hasItemMeta() || !item.getItemMeta().hasLore() || item.getType() == toAdd.getType())
					continue;
				
				List<String> lore = item.getItemMeta().getLore();
				
				for (CEnchantment ce : EnchantManager.getEnchantments(lore))
				{
					if (ce instanceof Insanity)
						continue;
					ce.getPotionEffectsOnWear().keySet().stream().filter(potionsToRemove::contains).forEach(potionsToRemove::remove);
				}
			}
			
			potionsToRemove.stream().filter(toCheck::hasPotionEffect).forEach(toCheck::removePotionEffect);
			
		}
		else
		{
			for (String line : toAdd.getItemMeta().getLore())
			{
				CEnchantment ce = EnchantManager.getEnchantment(line);
				int level = EnchantManager.getLevel(line);
				
				if (ce == null) continue;
				
				if (ce instanceof Feast)
				{
					toCheck.setFoodLevel(20);
					continue;
				}
				else if (ce instanceof Insanity)
				{
					ItemStack legChest = (toAdd.getType().toString().endsWith("CHESTPLATE")) ? toCheck.getInventory().getLeggings() : toCheck.getInventory().getChestplate();
					
					if (legChest == null || legChest.getType() == Material.AIR || !legChest.hasItemMeta() || !legChest.getItemMeta().hasLore())
						return;
					
					boolean shouldReturn = true;
					
					for (String loreLine : legChest.getItemMeta().getLore())
					{
						if (EnchantManager.containsEnchantment(loreLine, new Insanity()))
							shouldReturn = false;
					}
					
					if (shouldReturn)
						continue;
				}
				
				for (Entry<PotionEffectType, Integer> entry : ce.getPotionEffectsOnWear().entrySet())
				{
					toCheck.addPotionEffect(new PotionEffect(entry.getKey(), 600000, entry.getValue() + level), true);
				}
			}
		}
	}
	
	static void handleEvent(Player toCheck, Event e, HashSet<CEBasic> list)
	{
		if (Terminate.terminated.contains(toCheck.getUniqueId()))
			return;
		
		for (ItemStack item : toCheck.getInventory().getArmorContents())
		{
			if ((item != null) && (item.getType() != Material.AIR))
			{
				handleEventMain(item, e, list);
			}
		}
		handleEventMain(toCheck.getItemInHand(), e, list);
	}
	
	private static void handleEventMain(ItemStack i, Event e, HashSet<CEBasic> list)
	{
		if (list.isEmpty())
			return;
		
		if (i == null || !i.hasItemMeta())
			return;
		
		ItemMeta im = i.getItemMeta();
		
		if (!im.hasLore())
			return;
		
		List<String> lore = im.getLore();
		
		for (CEBasic cb : list)
		{
			CEnchantment ce = (CEnchantment) cb;
			if (Utils.isApplicable(i, ce))
			{
				lore.stream().filter(line -> EnchantManager.containsEnchantment(line, ce)).forEachOrdered(line ->
				{
					int level = EnchantManager.getLevel(line);
					ce.effect(e, i, level);
				});
				
			}
		}
	}
	
	static void handleEnchanting(final InventoryClickEvent event)
	{
		if (event.getView() == null)
			return;
		
		Player p = (Player) event.getWhoClicked();
		
		if (event.getRawSlot() >= 0 && event.getRawSlot() < 3)
		{
			final Inventory inv = event.getView().getTopInventory();
			
			ItemStack item = event.getCursor();
			ItemStack current = event.getCurrentItem();
			
			event.setCancelled(true);
			
			if (event.isShiftClick())
			{
				if (event.getSlot() != 2)
				{
					if (event.getView().getBottomInventory().firstEmpty() != -1)
					{
						event.getView().getBottomInventory().addItem(event.getCurrentItem());
						event.setCurrentItem(new ItemStack(Material.AIR));
						updateEnchantingInventory(inv, p);
					}
					return;
				}
			}
			
			switch (event.getSlot())
			{
				case 0:
					if (item != null && item.getAmount() > 1)
					{
						item.setAmount(item.getAmount() - 1);
						event.getWhoClicked().setItemOnCursor(item.clone());
						if (current != null && !current.getType().equals(Material.AIR))
							event.getWhoClicked().getInventory().addItem(current);
						item.setAmount(1);
					}
					else
					{
						event.getWhoClicked().setItemOnCursor(current);
					}
					
					inv.setItem(0, item);
					updateEnchantingInventory(inv, p);
					break;
				case 1:
					if (item == null || item.getType() == Material.AIR || EnchantManager.isEnchantmentBook(item))
					{
						inv.setItem(1, item);
						event.getWhoClicked().setItemOnCursor(current);
						updateEnchantingInventory(inv, p);
					}
					break;
				case 2:
					final ItemStack result = inv.getItem(2);
					
					if (result == null || result.getType() == Material.AIR || result.getType() == Material.BARRIER)
						break;
					
					inv.clear();
					new BukkitRunnable() {
						@Override
						public void run()
						{
							event.getWhoClicked().setItemOnCursor(result);
						}
					}.runTaskLater(CEMain.plugin, 1L);
					break;
			}
			
		}
		else
		{
			if (event.isShiftClick() && event.getCurrentItem() != null)
			{
				event.setCancelled(true);
				Inventory top = event.getView().getTopInventory();
				ItemStack current = event.getCurrentItem().clone();
				ItemStack topItem = current.clone();
				topItem.setAmount(1);
				
				if (EnchantManager.isEnchantmentBook(current))
				{
					if (current.getAmount() > 1)
					{
						current.setAmount(current.getAmount() - 1);
					}
					else if (current.getAmount() == 1)
					{
						current.setType(Material.AIR);
					}
					if (top.getItem(1) == null || top.getItem(1).getType().equals(Material.AIR))
					{
						top.setItem(1, topItem);
						event.setCurrentItem(current);
						updateEnchantingInventory(top, p);
					}
					else if (top.getItem(0) == null || top.getItem(0).getType().equals(Material.AIR))
					{
						top.setItem(0, topItem);
						event.setCurrentItem(current);
						updateEnchantingInventory(top, p);
					}
				}
				else if (EnchantManager.isEnchantable(current.getType().toString()))
				{
					if (current.getAmount() > 1)
					{
						current.setAmount(current.getAmount() - 1);
					}
					else if (current.getAmount() == 1)
					{
						current.setType(Material.AIR);
					}
					if ((top.getItem(0) == null || top.getItem(0).getType().equals(Material.AIR)))
					{
						top.setItem(0, topItem);
						event.setCurrentItem(current);
						updateEnchantingInventory(top, p);
					}
				}
			}
		}
	}
	
	static void updateEnchantingInventory(final Inventory inv, final Player p)
	{
		new BukkitRunnable() {
			
			@Override
			public void run()
			{
				ItemStack top = inv.getItem(0);
				ItemStack bot = inv.getItem(1);
				
				if (top == null || bot == null || top.getType().equals(Material.AIR) || bot.getType().equals(Material.AIR) || top.getType().equals(Material.ENCHANTED_BOOK))
				{
					inv.setItem(2, new ItemStack(Material.AIR));
					return;
				}
				
				if (EnchantManager.isEnchantmentBook(bot))
				{
					ItemStack item = top.clone();
					List<String> lore = top.getItemMeta().getLore();
					HashMap<CEnchantment, Integer> topList = EnchantManager.getEnchantmentLevels(new ArrayList<>(EnchantManager.getEnchantments(lore)), top.getItemMeta().getLore());
					HashMap<CEnchantment, Integer> botList = EnchantManager.getEnchantmentLevels(new ArrayList<>(EnchantManager.getEnchantments()), bot.getItemMeta().getLore());
					
					for (CEnchantment ce : botList.keySet())
					{
						if (Utils.isApplicable(item, ce))
						{
							int newLevel = botList.get(ce);
							int potentialLevel = 0;
							
							if (topList.containsKey(ce))
							{
								potentialLevel = topList.get(ce);
								
								if (potentialLevel < ce.getEnchantmentMaxLevel())
									topList.remove(ce);
							}
							
							item = EnchantManager.addEnchant(item, ce, newLevel + potentialLevel, p);
						}
					}
					
					ItemStack barrier = new ItemStack(Material.BARRIER);
					ItemMeta im = barrier.getItemMeta();
					
					if (EnchantManager.getEnchantments(item.getItemMeta().getLore()).size() > topList.size())
					{
						inv.setItem(2, item);
					}
					else if (item.getType() != Material.BARRIER)
					{
						im.setDisplayName(ChatColor.DARK_RED + "Incompatible Enchantment");
						barrier.setItemMeta(im);
						inv.setItem(2, barrier);
					}
					else
					{
						im.setDisplayName(ChatColor.DARK_RED + "Isufficent Permissions");
						barrier.setItemMeta(im);
						inv.setItem(2, barrier);
					}
					return;
				}
				inv.setItem(2, new ItemStack(Material.AIR));
			}
		}.runTaskLater(CEMain.plugin, 2);
	}
}

